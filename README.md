personal
========

personal website

Tech Blogs
==========
1. [CSDN](http://www.csdn.net)
2. [Jobbole](http://blog.jobbole.com/)
3. [ImportNews](http://www.importnew.com/)
4. [SegmentFault](http://segmentfault.com/)
5. [Kuqin](http://www.kuqin.com)

Python
======
1. [Code, programs, tips and tricks](http://love-python.blogspot.ca/)
2. [Coder Who Says Py](http://sayspy.blogspot.ca/)
3. [Real Python](https://realpython.com/)
